import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking

fun main(args: Array<String>) = runBlocking<Unit> {
    launch {
        delay(1000L)
        println("World!")
    }
    println("Hello, ")
    delay(2000L)

}