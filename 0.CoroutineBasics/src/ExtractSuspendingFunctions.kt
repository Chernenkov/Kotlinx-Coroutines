import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking

fun main(args: Array<String>) = runBlocking<Unit>{
    val job = launch {sayWorld()}
    print("Hello, ")
    job.join()
}

suspend fun sayWorld() = print("World!") // suspending function can be used inside coroutines