import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking

fun main(args: Array<String>){
    launch{
        delay(1000L)
        println("World!")
    }
    println("Hello, ")
    runBlocking { // runs blocking coroutine: it blocks main thread until code of coroutine
        // finishes
        delay(2000L) // // main thread blocks until code in this block finishes
        // when delay function is over after 2sec, main thread is alive again(not suspended)
    }
    //
    //
    //
    runBlocking {
        print("H")
        delay(1000L)
        runBlocking {
            print("ell")
            delay(1000L)
            runBlocking {
                print("o!")
                delay(1000L)
            }
        }
    }
    //
    //
    //
}