import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking

fun main(args: Array<String>) = runBlocking {
    val jobs = List(100_000){
        launch { doJob() }
    }
    jobs.forEach { it.join() }
}

suspend fun doJob(){
    delay(1000L)
    print("#")
}