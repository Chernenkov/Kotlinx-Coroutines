import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking

fun main(args: Array<String>) = runBlocking{
    launch {
        repeat(1000){i ->
            println("Sleeping #$i time...")
            delay(500L)
        }
    }
    delay(4000L)
}