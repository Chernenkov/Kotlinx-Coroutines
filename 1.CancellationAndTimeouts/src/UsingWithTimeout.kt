import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.runBlocking
import kotlinx.coroutines.experimental.withTimeout
import kotlinx.coroutines.experimental.withTimeoutOrNull

fun main(args: Array<String>) = runBlocking {
    // this function just throws TimeoutCancellationException
    // which is just a subclass of CancellationException
//    withTimeout(1300L) {
//        repeat(100) { i ->
//            println("Sleeping for #$i time...")
//            delay(500L)
//        }
//    }
    // but it throws exception so we need to use try-catch-finally
    // or we can use withTimeoutOrNull which returns null
    // on timeout instead of throwing TimeoutCancellationException
    val result = withTimeoutOrNull(2000L){ // change delay to bigger and result will be 'Done'
        repeat(10){ i -> // we can change it to smaller, the result will be 'Done'
            println("Sleeping for #$i time...")
            delay(500L) // change this to smaller and result will be 'Done'
        }
        "Done"
    }
    println("Result = $result")
}