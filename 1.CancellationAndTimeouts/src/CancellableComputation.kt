import kotlinx.coroutines.experimental.cancelAndJoin
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking

fun main(args: Array<String>) = runBlocking {
    val startTime = System.currentTimeMillis()
    val job = launch {
        var nextPrintTime = startTime
        var i = 0
//        while(i < 5){
        while(isActive) { // is true while coroutine is not completed and not cancelled yet
            if((System.currentTimeMillis() - nextPrintTime) >= 500L){
                println("Sleeping for #${i++} time...")
                nextPrintTime += 500L
            }
        }
    }
    delay(1300L) // during this delay the background [job] works
    println("MAIN is waiting...")
    job.cancelAndJoin() // as we use isActive as condition in while cycle, now coroutine will
    // be off when this method is called
    println("MAIN is quitting now...")
}