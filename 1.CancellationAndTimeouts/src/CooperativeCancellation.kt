import kotlinx.coroutines.experimental.cancelAndJoin
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking

fun main(args: Array<String>) = runBlocking {
    val startTime = System.currentTimeMillis()
    val job = launch {
        var nextPrintTime = startTime
        var i = 0
        while(i < 5){
            if((System.currentTimeMillis() - nextPrintTime) >= 500L){
                println("Sleeping for #${i++} time")
                nextPrintTime += 500L
            }

        }
    }
    delay(1200L) // during this delay the background [job] works
    println("MAIN is waiting...")
    job.cancelAndJoin()
    println("now, MAIN is quitting!")
}