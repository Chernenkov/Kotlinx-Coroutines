import kotlinx.coroutines.experimental.cancelAndJoin
import kotlinx.coroutines.experimental.delay
import kotlinx.coroutines.experimental.launch
import kotlinx.coroutines.experimental.runBlocking

fun main(args: Array<String>) = runBlocking{
    val job = launch {
        repeat(1000) { i ->
            println("Going to sleep for #$i time...")
            delay(500L)
        }
    }
    delay(1500L) // during this delay the background [job] works
    println("MAIN is waiting!!1!1")
//    job.cancel()
//    job.join()
    job.cancelAndJoin()
    println("Quitting!")
}