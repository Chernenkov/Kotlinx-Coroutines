import kotlinx.coroutines.experimental.*

fun main(args: Array<String>) = runBlocking {
    val job = launch {
        try{
            repeat(100){ i ->
                println("Sleeping for #$i time")
                delay(500L)
            }
        }catch (cancexc: CancellationException){
            // in this block coroutine is already cancelled
            // but if we need to call a suspending function (delay for example)
            // we can wrap the suspending code into withContext(){}
            withContext(NonCancellable){
                delay(1000L)
                println("Just delayed for 1s after requested to be cancelled...")
            }
        }
    }
    delay(1300L)
    println("MAIN is waiting...")
    job.cancelAndJoin()
    println("MAIN is now quitting!")
}