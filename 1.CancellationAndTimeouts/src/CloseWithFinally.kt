import kotlinx.coroutines.experimental.*

fun main(args: Array<String>) = runBlocking {
    val job = launch {
        try{
            repeat(100){ i ->
                println("Sleeping for #$i time")
                delay(500L)
            }
        }catch (cancexc: CancellationException){
            println("I got cancelled...")
        }
// finally { // finally does the same: catches the CancellationException
//            println("I got cancelled maybe...")
//        }
    }
    delay(600L)
    println("MAIN is waiting...")
    job.cancelAndJoin()
    println("MAIN is now quitting!")
}